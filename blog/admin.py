from django.contrib import admin

from .models import Post, Comment

# Register your models here.


class CommentInline(admin.TabularInline):

    model = Comment
    extra = 2


class PostAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,
         {'fields':
          ['title', 'slug', 'author', 'status', 'publish', 'tags', 'body', ]}),
    ]
    list_display = ['title', 'author', 'slug', 'status', 'publish', ]
    search_fields = ['title', 'body', ]
    list_filter = ['status', 'publish', 'created', 'updated', 'author', ]
    raw_id_fields = ['author', ]
    date_hierarchy = 'publish'
    ordering = ['status', 'publish', ]
    prepopulated_fields = {'slug': ['title', ]}
    inline = [
        CommentInline,
    ]


class CommentAdmin(admin.ModelAdmin):

    list_display = ('name', 'email', 'post', 'created', 'active')
    list_filter = ('active', 'created', 'updated')
    search_fields = ('name', 'email', 'body')


admin.site.register(Post, PostAdmin)
admin.site.register(Comment, CommentAdmin)
