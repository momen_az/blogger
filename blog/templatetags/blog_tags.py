from django import template
from django.db.models import Count
from ..models import Post


register = template.Library()


@register.simple_tag
def total_posts():
    return Post.objects.count()


@register.inclusion_tag('latest_posts.html')
def get_latest_posts(count):
    latest_posts = Post.objects.order_by('-publish')[:count]
    return {
        'latest_posts': latest_posts,
    }


@register.simple_tag
def get_most_commented_posts(count=4):
    context = Post.objects.annotate(
        total_comments=Count('comments')
    ).order_by('-total_comments', '-publish')[:count]
    return context


@register.filter(name='range')
def filter_range(start, end):
    return range(start, end+1)
