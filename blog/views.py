from django.shortcuts import render, get_object_or_404
from django.views.generic import ListView, CreateView, DetailView
from django.views.generic.edit import UpdateView, DeleteView
from django.urls import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.mail import send_mail
from django.http import HttpResponseRedirect
from django.db.models import Count

from .models import Post, Comment
from .forms import PostShareForm, CommentForm
from taggit.models import Tag

# Create your views here.


def PostShareView(request, pk):
    post = get_object_or_404(Post, pk=pk)
    sent = False

    if request.method == 'POST':
        form = PostShareForm(request.POST)
        if form.is_valid() == True:
            cd = form.cleaned_data
            # send email
            post_url = request.build_absolute_uri(post.get_absolute_url())

            subject = "{} ({}) recommend you to read {}".format(
                cd['name'],
                cd['email'], post.title)

            message = "Read {} at {} \n\n\{}'s comments:{}".format(
                post.title, post_url, cd['name'], cd['comments'])

            send_mail(subject, message, 'salezarop@gmail.com', [cd['to']])
            sent = True
    else:
        form = PostShareForm()

    context = {
        'post': post,
        'form': form,
        'sent': sent,
    }
    return render(request, 'post_share.html', context)


def PostListView(request, tag_slug=None):
    tag = None

    if request.method == 'POST':
        tag_slug = request.POST['tag_slug']
        url = "%s/" % (tag_slug)
        return HttpResponseRedirect(url)
    elif tag_slug:
        tag = get_object_or_404(Tag, slug=tag_slug)
        posts_list = Post.objects.filter(tags__in=[tag])
    else:
        posts_list = Post.objects.all()

    paginator = Paginator(posts_list, 4)
    page = request.GET.get('page')
    posts = paginator.get_page(page)
    context = {
        'posts': posts,
        'tag': tag,
    }

    return render(request, 'posts_list.html', context)


def PostDetailView(request, pk):
    post = get_object_or_404(Post, id=pk)
    comments = post.comments.filter(active=True)
    new_comment = None

    post_tags_ids = post.tags.values_list('id', flat=True)
    similar_posts = Post.objects.filter(tags__in=post_tags_ids)\
                                .exclude(id=post.id)
    similar_posts = similar_posts.annotate(same_tags=Count('tags'))\
        .order_by('-same_tags', '-publish')[:4]

    if request.method == 'POST':
        comment_form = CommentForm(data=request.POST)
        if comment_form.is_valid():
            new_comment = comment_form.save(commit=False)
            new_comment.post = post
            new_comment.save()
            comment_form = CommentForm()
    else:
        comment_form = CommentForm()

    context = {
        'post': post,
        'comments': comments,
        'new_comment': new_comment,
        'comment_form': comment_form,
        'similar_posts': similar_posts,
    }
    return render(request, 'post_detail.html', context)


# class PostListView(ListView):
#     model = Post
#     template_name = "posts_list.html"
#     context_object_name = "posts"
#     paginate_by = 4


# class PostDetailView(DetailView):
#     model = Post
#     template_name = "post_detail.html"
