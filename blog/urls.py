from django.urls import path

from .views import PostDetailView, PostListView, PostShareView

app_name = 'blog'
urlpatterns = [
    path('', PostListView, name='posts_list'),
    path('<slug:tag_slug>/', PostListView, name='posts_list_by_tag'),
    path('<int:pk>/detail/', PostDetailView, name='post_detail'),
    path('<int:pk>/share/', PostShareView, name='post_share'),
]
